Provisionator README

This module is still very much a work in progress. It's also likely quite rough, but it works.

It assumes that you'll be running in a shared Drupal hosting environment, with Drupal itself not installed directly within the Apache wwwroot directory. It assumes new sites will be exposed to Apache by creating symlinks from the Drupal directory to locations within wwwroot.

- D'Arcy Norman
	dlnorman@ucalgary.ca