<?php

// PROVISIONATOR CLIENT NODE FUNCTIONS

/**
 * Implementation of hook_form().
 */
function provisionatorclient_form(&$node, &$param) {
	$form['title'] = array(
	                     '#type' => 'textfield',
	                     '#title' => t('Title'),
	                     '#required' => TRUE,
	                     '#default_value' => $node->title,
	                     '#weight' => -5
	                 );

	$form['body_filter']['body'] = array(
	                                   '#type' => 'textarea',
	                                   '#title' => t('Description'),
	                                   '#default_value' => $node->body,
	                                   '#required' => TRUE
	                               );

	$form['body_filter']['filter'] = filter_form($node->format);

	$form['domainprefix'] = array(
	                            '#type' => 'textfield',
	                            '#title' => t('Domain Prefix'),
	                            '#required' => TRUE,
	                            '#default_value' => $node->domainprefix,
	                            '#description' => t('The first part of the domain (URL) to be used for sites created for this client. Could be something like www.server.com/sites - all sites will be added after this prefix')
	                        );

	$form['symlinkpath'] = array(
	                           '#type' => 'textfield',
	                           '#title' => t('Symlink Path'),
	                           '#default_value' => $node->symlinkpath,
	                           '#required' => TRUE,
	                           '#description' => t('Directory to contain the symlink from Drupal, which is used to expose the deployed site to Apache.')
	                       );

	$form['dbprefix'] = array(
	                        '#type' => 'textfield',
	                        '#title' => t('Database Prefix'),
	                        '#default_value' => $node->dbprefix,
	                        '#required' => TRUE,
	                        '#description' => t('Prefix to use before database names in MySQL. Could be something like drupalsites_')
	                    );

	$form['apachetargetdir'] = array(
		'#type' => 'textfield',
		'#title' => t('Apache Target Directory'),
		'#default_value' => $node->apachetargetdir,
		'#required' => TRUE,
		'#description' => t('Filesystem path of the directory to contain the symlink used by Apache to serve this site.')
	);

	// TODO: Enter additional form elements

	return $form;
}


/**
 * Implementation of hook_insert().
 */
function provisionatorclient_insert( $node ) {
	db_query("INSERT INTO {provisionatorclient} (nid, domainprefix, symlinkpath, dbprefix, apachetargetdir) VALUES (%d, '%s', '%s', '%s', '%s')", $node->nid, $node->domainprefix, $node->symlinkpath, $node->dbprefix, $node->apachetargetdir);
}

/**
 * Implementation of hook_load().
 */
function provisionatorclient_load( $node ) {
	$additions = db_fetch_object(db_query('SELECT domainprefix, symlinkpath, dbprefix, apachetargetdir FROM {provisionatorclient} WHERE nid = %d', $node->nid));
	return $additions;
}

/**
 * Implementation of hook_delete().
 */
function provisionatorclient_delete( $node ) {
	db_query("DELETE FROM {provisionatorclient} WHERE nid = %d", $node->nid);
}

/**
 * Implementation of hook_prepare().
 */
function provisionatorclient_prepare(&$node) {

}

/**
 * Implementation of hook_submit().
 */
function provisionatorclient_submit(&$node) {

}

/**
 * Implementation of hook_update().
 */
function provisionatorclient_update($node) {
	if ($node->revision) {
		provisionatorclient_insert($node);
	}
	else {
		// TODO: Enter database update query here, for example:
		// db_query("UPDATE {node_example} SET color = '%s', quantity = %d WHERE vid = %d", $node->color, $node->quantity, $node->vid);
		db_query("UPDATE {provisionatorclient} SET domainprefix = '%s', symlinkpath = '%s', dbprefix = '%s', apachetargetdir = '%s' WHERE nid = %d", $node->domainprefix, $node->symlinkpath, $node->dbprefix, $node->apachetargetdir, $node->nid);
	}
}

/**
 * Implementation of hook_validate().
 */
function provisionatorclient_validate(&$node) {
	// TODO: Enter form validation code here
}

/**
 * Implementation of hook_view().
 */
function provisionatorclient_view(&$node, $teaser = FALSE, $page = FALSE) {
	// TODO: Insert additional code (call to theme functions, etc.) to execute when viewing a node, for example:
	// $node = node_prepare($node, $teaser);
	// $order_info = theme('node_example_order_info', $node);
	// $node->body .= $order_info;
	// $node->teaser .= $order_info;

	$node = node_prepare( $node, $teaser );

	$client_info = '';
	$client_info .= t('<br />Domain Prefix: ').$node->domainprefix;
	$client_info .= t('<br />Symlink Path: ').$node->symlinkpath;
	$client_info .= t('<br />DB Prefix: ').$node->dbprefix;
	$client_info .= t('<br />Apache Symlink Directory: ').$node->apachetargetdir;
	$node->body .= $client_info;
	$node->teaser .= $client_info;
}

?>