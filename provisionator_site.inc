<?php 

// PROVISIONATOR SITE NODE FUNCTIONS

include_once('provisionator_helper.inc');

/**
 * Implementation of hook_access().
 */
function provisionatorsite_access($op, $node) {
	global $user;

	if ($op == 'create') {
		return user_access('deploy new site');
	}

	if ($op == 'update') {
		if ((user_access('edit own deployed sites') && ($user->uid == $node->uid)) || (user_access('administer deployed sites'))) {
			return TRUE;
		}
	}

	if ($op == 'delete') {
		if ((user_access('delete own deployed sites') && ($user->uid == $node->uid)) || (user_access('administer deployed sites'))) {
			return TRUE;
		}
	}
}


/**
 * Implementation of hook_delete().
 */
function provisionatorsite_delete(&$node) {
	// TODO: Enter database deletion query here, for example:
	// db_query('DELETE FROM {node_example} WHERE nid = %d', $node->nid);
	db_query("DELETE FROM {provisionatorsite} WHERE nid = %d", $node->nid);
}


/**
 * Implementation of hook_form().
 */
function provisionatorsite_form(&$node, &$param) {
	$form['title'] = array(
						 '#type' => 'textfield',
						 '#title' => t('Title'),
						 '#required' => TRUE,
						 '#default_value' => $node->title,
						 '#weight' => -5
					 );

	$form['body_filter']['body'] = array(
		'#type' => 'textarea',
		'#title' => t('Description'),
		'#default_value' => $node->body,
		'#required' => TRUE
								   );

	$form['body_filter']['filter'] = filter_form($node->format);

	$form['shortname'] = array(
				'#type' => 'textfield',
				'#title' => t('Shortname'),
				'#required' => TRUE,
				'#default_value' => $node->shortname,
				'#description' => t('The short name for the site, used in the url (eg. www.server.com/shortname) and in the database name.')
			);

	$form['status'] = array(
				'#type' => 'select',
				'#title' => t('Status'),
				'#options' => getStatusArray(),
				'#default_value' => $node->sitestatus,
				'#description' => t('Status of the site - select an option.')
			);

	$form['fullurl'] = array(
				'#type' => 'textfield',
				'#title' => t('Full URL of site'),
				'#default_value' => $node->fullurl,
				'#required' => FALSE,
				'#description' => t('Full, qualified URL of the site. If you open the site in a browser, this is the address. Leave empty to use defaults from Client and shortname.')
			);
			
	$form['profile'] = array(
		'#type' => 'select',
		'#title' => t('Site Profile'),
		'#default_value' => $node->profile,
		'#required' => TRUE,
		'#description' => t('Which database template should be used when setting up this site?'),
		'#options' => provisionator_get_profiles(),
	);

	$form['client'] = array(
				'#type' => 'select',
				'#title' => t('Client'),
				'#default_value' => $node->clientid,
				'#options' => getClientListOptions(),
				'#description' => t('Who is the site for? This will be associated with the Provisionator Client node, and will cause particular settings to be inherited by this site (eg. URL prefix, database prefix, etc...)')
			);


	// TODO: Enter additional form elements

	return $form;
}

function getStatusArray() {
	$statusarray = array(
		t('Active'), 
		t('Static'), 
		t('Abandoned'), 
		t('Freeze Dried'), 
		t('Disabled')
	);
	return $statusarray;
}

// this function should query all nodes of type provisionatorclient and return them in alpha order
function getClientListOptions() {
	$clientsql = "SELECT title, nid FROM {node} WHERE type = 'provisionatorclient'";
	$result = db_query($clientsql);
	$options = array();
	
	while ($anode = db_fetch_object($result)) {
		$clientid = $anode->nid;
		$options[$clientid] = $anode->title;
	}
	return $options;
}

/**
 * Implementation of hook_insert().
 */
function provisionatorsite_insert($node) {

	// check for fullurl. if not provided, generate it.
	// settings pulled from the provisionatorclient node
	// try to load the client node for the given clientid
	$client = db_fetch_object(db_query("SELECT dbprefix, symlinkpath, domainprefix from {provisionatorclient} where nid = %d", $node->client));
	
	// check to see if a fullurl has (not) been provided. In that case, generate one from the client and site data.
	if (($node->fullurl == NULL) || ($node->fullurl == '')) {
		$node->fullurl = 'http://'.$client->domainprefix . '/' . $node->shortname;
	}
	
	// first, try to provision the site
	$deployresult = provisionator_deploy_site( $node );
	
	// successful? insert that sucker.
	if ($deployresult == TRUE) {
		db_query("INSERT INTO {provisionatorsite} (nid, shortname, sitestatus, fullurl, profile, clientid) VALUES (%d, '%s', %d, '%s', '%s', %d)", $node->nid, $node->shortname, $node->sitestatus, $node->fullurl, $node->profile, $node->client);
	} else {
		drupal_set_message(t('Error deploying the site. Record not saved.'));
	}
}


/**
 * Implementation of hook_load().
 */
function provisionatorsite_load($node) {
	// TODO: Obtain and return additional fields added to the node type, for example:
	// $additions = db_fetch_object(db_query('SELECT color, quantity FROM {node_example} WHERE vid = %d', $node->vid));
	// return $additions;
	$additions = db_fetch_object(db_query('SELECT shortname, sitestatus, fullurl, profile, clientid FROM {provisionatorsite} WHERE nid = %d', $node->nid));
		
	return $additions;
}

/**
 * Implementation of hook_prepare().
 */
function provisionatorsite_prepare(&$node) {

}

/**
 * Implementation of hook_submit().
 */
function provisionatorsite_submit(&$node) {

}

/**
 * Implementation of hook_update().
 */
function provisionatorsite_update($node) {
	if ($node->revision) {
		provisionatorsite_insert($node);
	}
	else {
		// TODO: Enter database update query here, for example:
		// db_query("UPDATE {node_example} SET color = '%s', quantity = %d WHERE vid = %d", $node->color, $node->quantity, $node->vid);
		db_query("UPDATE {provisionatorsite} SET shortname = '%s', sitestatus = %d, fullurl = '%s', clientid = %d WHERE nid = %d", $node->shortname, $node->sitestatus, $node->fullurl, $node->client, $node->nid);
		drupal_set_message('status: ' . $node->sitestatus);
	}
}

/**
 * Implementation of hook_validate().
 */
function provisionatorsite_validate(&$node) {
	// TODO: Enter form validation code here
}

/**
 * Implementation of hook_view().
 */
function provisionatorsite_view(&$node, $teaser = FALSE, $page = FALSE) {
	// TODO: Insert additional code (call to theme functions, etc.) to execute when viewing a node, for example:
	// $node = node_prepare($node, $teaser);
	// $order_info = theme('node_example_order_info', $node);
	// $node->body .= $order_info;
	// $node->teaser .= $order_info;

	$node = node_prepare( $node, $teaser );

	// try to load the client node for the given clientid
	$client_result = db_fetch_object(db_query("SELECT title from {node} where nid = %d", $node->clientid));
	
	$statusarray = getStatusArray();

	$site_info = '';
	$site_info .= t('<br />Site Shortname: ').$node->shortname;
	$site_info .= t('<br />Status: ').$statusarray[$node->sitestatus];
	$site_info .= t('<br />Full URL: ').$node->fullurl;
	$site_info .= t('<br />Client: '.$client_result->title);

	$node->body .= $site_info;
	$node->teaser .= $site_info;
}


?>