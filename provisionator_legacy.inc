<?php

function provisionator_page() {
	$output = t('');

	$output .= t('<p>This module provides utilities to easily create, deploy, and manage sites in a shared Drupal hosting environment.</p>');

	// DEPLOY NEW SITE
	$form['deploy'] = array(
	                      '#type' => 'fieldset',
	                      '#title' => t('Deploy new site'),
	                      '#tree' => TRUE,
	                      '#collapsible' => TRUE,
	                      '#collapsed' => TRUE,
	                  );

	$form['deploy']['sitename'] = array(
	                                  '#type' => 'textfield',
	                                  '#title' => 'Site Name',
	                                  '#size' => 30,
	                                  '#maxlength' => 255,
	                                  '#required' => TRUE,
	                                  '#default_value' => variable_get('sitename',''),
	                                  '#description' => t('The title for the site, to be used in the site, and in lists of Drupal sites.'),
	                              );

	$form['deploy']['shortname'] = array(
	                                   '#type' => 'textfield',
	                                   '#title' => 'Short Name',
	                                   '#size' => 30,
	                                   '#maxlength' => 64,
	                                   '#required' => TRUE,
	                                   '#default_value' => variable_get('shortname',''),
	                                   '#description' => t('The shortname for the site, to be used in the URL and database.'),
	                               );

	$profiles = drupal_map_assoc(getProvisionatorProfiles(), NULL);
	$form['deploy']['profile'] = array(
	                                 '#type' => 'select',
	                                 '#title' => t('Profile'),
	                                 '#required' => TRUE,
	                                 '#default_value' => variable_get('profile', ''),
	                                 '#options' => $profiles,
	                                 '#description' => t('Site profile for new site.'),
	                             );

	$form['deploy']['notes'] = array(
	                               '#type' => 'textarea',
	                               '#title' => 'Notes',
	                               '#required' => FALSE,
	                               '#default_value' => variable_get('notes',''),
	                               '#description' => t('Some notes. Who is the site for? Anything to keep in mind?.'),
	                           );

	$form['deploy']['submit'] = array('#type' => 'submit', '#value' => t('Deploy'));

	$output .= drupal_get_form('provisionator_page', $form);


	// LIST EXISTING SITES, PROVIDE FUNCTIONS FOR EACH
	// display code borrowed/modified from watchdog.module
	$header = array(
	              array('data' => t('Site Name'), 'field' => 'p.sitename'),
	              array('data' => t('Date Deployed'), 'field' => 'p.timestamp', 'sort' => 'desc'),
	              array('data' => t('Profile'), 'field' => 'p.profile'),
	              array('data' => t('User'), 'field' => 'u.name'),
	              array('data' => t('Notes'), 'field' => 'p.notes'),
	              array('data' => t('Operations'), NULL)
	          );

	$sql = "SELECT *, u.name, u.uid FROM {provisionator_sites} p INNER JOIN {users} u ON p.uid = u.uid";
	$tablesort = tablesort_sql($header);
	$result = pager_query($sql . $tablesort, 100);

	while ($deployedsite = db_fetch_object($result)) {
		$rows[] = array('data' =>
		                        array(
		                            // Cells
		                            '<a href="/'.$deployedsite->shortname.'/">'.$deployedsite->sitename.'</a>',
		                            format_date($deployedsite->timestamp, 'small'),
		                            t($deployedsite->profile),
		                            theme('username', $deployedsite),
		                            t(truncate_utf8($deployedsite->notes, 56, TRUE, TRUE)),
		                            t('n/a'),
		                        ),
		                // Attributes for tr
		                'class' => "provisionator-". preg_replace('/[^a-z]/i', '-', $deployedsite->type)
		               );
	}

	if (!$rows) {
		$rows[] = array(array('data' => t('No sites deployed using Provisionator.'), 'colspan' => 6));
	}

	$manifest_table = theme('table', $header, $rows);
	$manifest_table .= theme('pager', NULL, 100, 0);

	$output .= $manifest_table;

	return $output;
}

function provisionator_page_validate( $form_id, $form_values ) {

	$shortname = $form_values['deploy']['shortname'];
	$dbprefix = variable_get('provisionator_dbprefix','');
	$drupal_site_dir_prefix = variable_get('provisionator_drupal_site_dir_prefix', '');
	$apache_target_dir = variable_get('provisionator_apache_target_dir','');
	$drupal_installed_path = variable_get('provisionator_drupal_installed_path', '');
	$mysqlbin = variable_get('provisionator_mysqlbin','');
	$mysqlhost = variable_get('provisionator_mysqlhost','');
	$mysqluser = variable_get('provisionator_mysqluser','');
	$mysqlpass = variable_get('provisionator_mysqlpass','');
	$dbname = $dbprefix.$shortname;


	// verify that the drupal site directory doesn't already exist
	$drupalsitedir = $drupal_installed_path.'/sites/'.$drupal_site_dir_prefix.'.'.$shortname;
	if (file_exists($drupalsitedir)) {
		$output .= 'Drupal already has a site configured at '.$drupalsitedir;
	}

	// verify that no database is using the shortname


	// verify that nothing lives at the symlink target
	$apachesymlink = $apache_target_dir.'/'.$shortname;
	if (file_exists($apachesymlink)) {
		$output .= '<br />Symlink already exists at '.$apachesymlink;
	}

	if ($output <> NULL) {
		form_set_error('', t($output));
	}
}

function provisionator_page_submit( $form_id, $form_values ) {
	// settings. All of these values should be customizable via admin/settings/provisionator
	$profilesdir =  variable_get('provisionator_profilesdir','');
	$mysqlbin = variable_get('provisionator_mysqlbin','');
	$mysqlhost = variable_get('provisionator_mysqlhost','');
	$mysqluser = variable_get('provisionator_mysqluser','');
	$mysqlpass = variable_get('provisionator_mysqlpass','');
	$dbprefix = variable_get('provisionator_dbprefix','');
	$drupal_site_template_dir = variable_get('provisionator_drupal_site_template_dir', '');
	$drupal_site_dir_prefix = variable_get('provisionator_drupal_site_dir_prefix', '');
	$apache_target_dir = variable_get('provisionator_apache_target_dir','');
	$drupal_installed_path = variable_get('provisionator_drupal_installed_path', '');
	$manifest_filename = variable_get('provisionator_manifest_index_filename', '');

	$output = 'Drupal site <a href="/' . $form_values['deploy']['shortname'] . '/">'. $form_values['deploy']['shortname'].'</a>';

	$shortname = $form_values['deploy']['shortname'];
	$dbname = $dbprefix.$shortname;
	//$output .= '<br />Creating database: ' .$dbname;

	$profile = $form_values['deploy']['profile'];

	$sqlfile = $profilesdir.'/'.$profile;

	// create the database
	$result = db_query( 'create database '.$dbname );

	// populate the database with the sql template file
	//$output .= ($mysqlbin.' -h '.$mysqlhost.' -u '.$mysqluser.' -p'.$mysqlpass.' '.$dbname.' < '.$sqlfile);
	exec($mysqlbin.' -h '.$mysqlhost.' -u '.$mysqluser.' -p'.$mysqlpass.' '.$dbname.' < '.$sqlfile);

	// copy the drupal site template directory
	$filescopied = copydir( $drupal_site_template_dir, $drupal_installed_path.'/sites/'.$drupal_site_dir_prefix.'.'.$shortname);

	// replace values in the new drupal site directory for this site
	$settingsfilename = $drupal_installed_path.'/sites/'.$drupal_site_dir_prefix.'.'.$shortname.'/settings.php';

	$settingsfilecontents = file_get_contents($settingsfilename);
	$settingsfilecontents = str_replace('database_name_goes_here', $dbname, $settingsfilecontents);
	$settingsfilecontents = str_replace('file_directory_path_goes_here', $drupal_site_dir_prefix.'.'.$shortname, $settingsfilecontents);

	$result = compat_file_put_contents($settingsfilename, $settingsfilecontents);

	// link the sitename into the web document root
	symlink( $drupal_installed_path, $apache_target_dir.'/'.$shortname);

	$output .= '<br />'.$form_values['deploy']['sitename'] . ' deployed. yay.';

	// now, do something to add a manifest record
	$notes = $form_values['deploy']['notes'];
	$sitename = $form_values['deploy']['sitename'];

	$status = 1; // only have one value for now. 1 == "active"

	global $user;

	db_query("INSERT INTO {provisionator_sites} (uid, status, timestamp, shortname, sitename, profile, notes) VALUES (%d, %d, %d, '%s', '%s', '%s', '%s')", $user->uid, $status, time(), $shortname, $sitename, $profile, $notes);

	watchdog('Provisionator', 'Site deployed: '.$shortname);


	// LIST EXISTING SITES, PROVIDE FUNCTIONS FOR EACH
	// display code borrowed/modified from watchdog.module
	$header = array(
	              array('data' => t('Site Name'), null),
	              array('data' => t('Date Deployed'), 'field' => 'p.timestamp', 'sort' => 'desc'),
	              array('data' => t('Profile'), null),
	              array('data' => t('User'), null),
	              array('data' => t('Notes'), null),
	          );

	$sql = "SELECT *, u.name, u.uid FROM {provisionator_sites} p INNER JOIN {users} u ON p.uid = u.uid";
	$tablesort = tablesort_sql($header);
	$result = pager_query($sql . $tablesort, 1000);

	while ($deployedsite = db_fetch_object($result)) {
		$rows[] = array('data' =>
		                        array(
		                            // Cells
		                            '<a href="/'.$deployedsite->shortname.'/">'.$deployedsite->sitename.'</a>',
		                            format_date($deployedsite->timestamp, 'small'),
		                            t($deployedsite->profile),
		                            theme('username', $deployedsite),
		                            t(truncate_utf8($deployedsite->notes, 56, TRUE, TRUE)),
		                        ),
		                // Attributes for tr
		                'class' => "provisionator-". preg_replace('/[^a-z]/i', '-', $deployedsite->type)
		               );
	}

	if (!$rows) {
		$rows[] = array(array('data' => t('No sites deployed using Provisionator.'), 'colspan' => 6));
	}

	$manifest_table = theme('table', $header, $rows);
	$manifest_table .= theme('pager', NULL, 100, 0);

	// write $manifest_table to the static index file
	$manifest_file_contents = '<html><head><title>Provisioned Sites</title></head><body>'.
	                          $manifest_file_contents .= $manifest_table;
	$manifest_file_contents .= '</body></html>';
	$result = compat_file_put_contents($manifest_filename, $manifest_file_contents);



	drupal_set_message( t( $output) );
}

function provisionator_deploy() {
	return t('Provisionator deploy page!');
}


// provide a page to edit a manifest record and/or perform operations on a site
function provisionator_managesite( ) {
	$output = 'manage a site and that site\'s manifest record...';
	drupal_set_message( t($output) );
}


/**
 * Implementation of hook_db_rewrite_sql().
 */
function provisionator_db_rewrite_sql($query, $primary_table, $primary_field, $args) {

}





?>