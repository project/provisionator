<?php
// custom functions


function copydir($original , $destination) {
	$file=opendir($original);
	if (file_exists($destination)){ return 0;}
	if( !mkdir($destination, fileperms($original)) ) {
		/* CAN'T CREATE THE DESTINATION directory! BORK APPROPRIATELY! */
		watchdog('Provisionator', 'Unable to create directory while copying file: '.$destination);
	}
	$total = 0;
	$l = array('.', '..');
	while ($afile = readdir($file)) {
		if (!in_array( $afile, $l)){
			if (is_dir($original."/".$afile)){
				$total += copydir("$original/$afile", "$destination/$afile");
			} else {
				copy("$original/$afile", "$destination/$afile");
				$total++;
			}
		}
	}
	return $total;
}

// this method is added because PHP's native file_put_contents() isn't available on all systems.
// ideally, this method should test for the existence of file_put_contents() and if available, use that. If not, fall back on fopen()...
function compat_file_put_contents($n,$d) {
	$f=@fopen($n,"w");
	if (!$f) {
		return false;
	} else {
		fwrite($f,$d);
		fclose($f);
		return true;
	}
}

function provisionator_get_profiles() {
	$ctr = 0;
	$path = variable_get('provisionator_profilesdir','');

	if ( $p_dir = @opendir($path) ) {
		while ( false !== ($p_file = readdir($p_dir)) ) {
			// add checks for other image file types here, if you like
			if ( preg_match("/(\.sql)$/", $p_file) ) {
				$profiles[$ctr] = $p_file;
				$ctr++;
			}
		}
		closedir($p_dir);
		return $profiles;
	} else {
		return false;
	}
}



function provisionator_deploy_site( $sitenode ) {
	// settings. All of these values should be customizable via admin/settings/provisionator
	$profilesdir =  variable_get('provisionator_profilesdir','');
	$mysqlbin = variable_get('provisionator_mysqlbin','');
	$mysqlhost = variable_get('provisionator_mysqlhost','');
	$mysqluser = variable_get('provisionator_mysqluser','');
	$mysqlpass = variable_get('provisionator_mysqlpass','');
	$drupal_site_template_dir = variable_get('provisionator_drupal_site_template_dir', '');
	$drupal_site_dir_prefix = variable_get('provisionator_drupal_site_dir_prefix', '');
	$drupal_installed_path = variable_get('provisionator_drupal_installed_path', '');
	$manifest_filename = variable_get('provisionator_manifest_index_filename', '');


	// settings pulled from the provisionatorsite node
	$shortname = $sitenode->shortname;
	$profilenumber = $sitenode->profile;
	$profiles = provisionator_get_profiles();
	$profile = $profiles[$profilenumber];
	
	
	// settings pulled from the provisionatorclient node
	// try to load the client node for the given clientid
	$client = db_fetch_object(db_query("SELECT dbprefix, symlinkpath, domainprefix, apachetargetdir from {provisionatorclient} where nid = %d", $sitenode->client));
	$dbprefix = $client->dbprefix;
	$apache_target_dir = $client->apachetargetdir;

	
	// settings derived from calculating/merging other settings
	$dbname = $dbprefix.$shortname;
	$sqlfile = $profilesdir.'/'.$profile;
	
	// create the database
	$result = db_query( 'create database '.$dbname );

	// populate the database with the sql template file
	exec($mysqlbin.' -h '.$mysqlhost.' -u '.$mysqluser.' -p'.$mysqlpass.' '.$dbname.' < '.$sqlfile);

	// convert fullurl to cleaned dot-syntax format
	$drupal_site_dir_name = str_replace('http://', '', $sitenode->fullurl);
	$drupal_site_dir_name = str_replace('/', '.', $drupal_site_dir_name);

	// copy the drupal site template directory
	$drupal_site_dir = $drupal_installed_path.'/sites/'.$drupal_site_dir_name; //$drupal_site_dir_prefix.'.'.$shortname;
	$filescopied = copydir( $drupal_site_template_dir, $drupal_site_dir);

	// replace values in the new drupal site directory for this site
	$settingsfilename = $drupal_site_dir.'/settings.php';

	$settingsfilecontents = file_get_contents($settingsfilename);
	$settingsfilecontents = str_replace('database_name_goes_here', $dbname, $settingsfilecontents);
	$settingsfilecontents = str_replace('file_directory_path_goes_here', $drupal_site_dir_name, $settingsfilecontents);
	$settingsfilecontents = str_replace('base_url_goes_here', $sitenode->fullurl, $settingsfilecontents);
	
	$result = compat_file_put_contents($settingsfilename, $settingsfilecontents);

	// link the sitename into the web document root
	symlink( $drupal_installed_path, $apache_target_dir.'/'.$shortname);

	//$output .= '<br />'.$sitenode->sitename . ' deployed. yay.';
	$output .= '<p><a href="'.$sitenode->fullurl.'" target="_blank">'.$sitenode->title.'</a> deployed successfully.</p>';
	
	watchdog('Provisionator', 'Site deployed: '.$shortname);


	drupal_set_message( t( $output) );
	
	return TRUE;
}



?>