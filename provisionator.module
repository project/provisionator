<?php

/**
 * @file
 * Provisionator is a module that allows administrators to easily deploy (or provision) new Drupal sites on a shared hosting environment.
 * It will take care of creating a database, populating it with a selected database profile, creating and modifying the Drupal site directory,
 * creating a symlink to expose the site to Apache (assuming Drupal is installed outside of the wwwroot of Apache), and (soon) will also
 * manage a database table to keep track of deployed sites. It will eventually provide additional functions, allowing admins to de-provision
 * sites, freeze-dry them as static html, and delete them from the system.
 */

include_once('provisionator_site.inc');
include_once('provisionator_client.inc');
include_once('provisionator_helper.inc');

// once functionality is migrated over, drop this include and delete the legacy.inc file
include_once('provisionator_legacy.inc');


/**
 * Implementation of hook_help().
 */
function provisionator_help($section) {
        switch ($section) {
        case 'admin/help#provisionator':
                        return t('help text goes here');
        case 'admin/modules#description':
                return t('Drupal site creation utility.');

        case 'node/add#provisionatorsite':
                return t('Use Provisionator to deploy a new Drupal site, using a specified template, and configuring it to be visible at an appropriate URL');

        case 'node/add#provisionatorclient':
                return t('Configure a client organization or institution, with custom settings for domain, etc...');
        }
}


/**
 * Implementation of hook_link().
 */
function provisionator_link($type, $node = NULL, $teaser = FALSE) {
        $links = array();

        // TODO: Perform logic to determine when link should appear
        $links[] = l(t('TODO: Fill in link title'), 'TODO: Fill in link path', array('title' => t('TODO: Fill in link title attribute.')));
        // OPTIONAL: Add additional links

        return $links;
}


/**
 * Implementation of hook_menu().
 */
function provisionator_menu($may_cache) {
        $items = array();

        if ($may_cache) {
                $items[] = array(
                                'path' => 'provisionator',
                                'title' => t('provisionator'),
                                'callback' => 'provisionator_page',
                                'access' => user_access('access provisionator'),
                                'description' => t('Provisionator (legacy interface)')
                        );

                $items[] = array(
                                'path' => 'node/add/provisionatorsite',
                                'title' => t('provisionator site'),
                                'access' => user_access('deploy new site')
                        );

                $items[] = array(
                                'path' => 'node/add/provisionatorclient',
                                'title' => t('provisionator client'),
                                'access' => user_access('administer provisionator')
                        );
        }

        return $items;
}


function provisionator_settings() {
        // only administrators can access this function
        if (!user_access('access provisionator')) {
                return t('No provisionator for you!');
        }

        $form['provisionator_mysqlhost'] = array(
                '#type' => 'textfield',
                '#title' => 'MySQL Server Host',
                '#size' => 30,
                '#maxlength' => 256,
                '#required' => TRUE,
                '#default_value' => variable_get('provisionator_mysqlhost','localhost'),
                '#description' => t('Server to use for MySQL.'),
         );

        $form['provisionator_mysqlbin'] = array(
                '#type' => 'textfield',
                '#title' => 'MySQL binary location',
                '#size' => 30,
                '#maxlength' => 64,
                '#required' => TRUE,
                '#default_value' => variable_get('provisionator_mysqlbin','/usr/bin/mysql'),
                '#description' => t('Location of the mysql command. Like /usr/bin/mysql'),
        );

        $form['provisionator_mysqluser'] = array(
                '#type' => 'textfield',
                '#title' => 'MySQL Username',
                '#size' => 30,
                '#maxlength' => 64,
                '#required' => TRUE,
                '#default_value' => variable_get('provisionator_mysqluser','mysqluser'),
                '#description' => t('MySQL Username for creating new databases.'),
         );

        $form['provisionator_mysqlpass'] = array(
                '#type' => 'textfield',
                '#title' => 'MySQL Password',
                '#size' => 30,
                '#maxlength' => 64,
                '#required' => TRUE,
                '#default_value' => variable_get('provisionator_mysqlpass','mysqlpassword'),
                '#description' => t('MySQL Password for creating new databases.'),
         );

        $form['provisionator_dbprefix'] = array(
                '#type' => 'textfield',
                '#title' => 'Database Prefix',
                '#size' => 30,
                '#maxlength' => 64,
                '#required' => TRUE,
                '#default_value' => variable_get('provisionator_dbprefix','drupal_'),
                '#description' => t('Prefix to prepend new database names.'),
        );

        $form['provisionator_profilesdir'] = array(
                '#type' => 'textfield',
                '#title' => 'Profiles SQL directory',
                '#size' => 30,
                '#maxlength' => 512,
                '#required' => TRUE,
                '#default_value' => variable_get('provisionator_profilesdir','./'),
                '#description' => t('Directory containing the .sql files for the profiles.'),
                                         );

        $form['provisionator_drupal_site_template_dir'] = array(
                '#type' => 'textfield',
                '#title' => 'Drupal Site template directory',
                '#size' => 30,
                '#maxlength' => 512,
                '#required' => TRUE,
                '#default_value' => variable_get('provisionator_drupal_site_template_dir','../../sites/template'),
                '#description' => t('Directory containing the template of the Drupal site directory.'),
                );

        $form['provisionator_drupal_site_dir_prefix'] = array(
                '#type' => 'textfield',
                '#title' => 'Drupal Site directory prefix',
                '#size' => 30,
                '#maxlength' => 512,
                '#required' => TRUE,
                '#default_value' => variable_get('provisionator_drupal_site_dir_prefix','localhost'),
                '#description' => t('Should match the Drupal site directory naming convention. Something like "blogs.bcit.ca".'),
                );

        $form['provisionator_drupal_installed_path'] = array(
                '#type' => 'textfield',
                '#title' => 'Drupal installation directory',
                '#size' => 30,
                '#maxlength' => 512,
                '#required' => TRUE,
                '#default_value' => variable_get('provisionator_drupal_installed_path','/Library/WebServer/Drupal'),
                '#description' => t('Where is Drupal installed on the server\'s filesystem? use that.'),
                );

        $form['provisionator_apache_target_dir'] = array(
                '#type' => 'textfield',
                '#title' => 'Apache target directory',
                '#size' => 30,
                '#maxlength' => 512,
                '#required' => TRUE,
                '#default_value' => variable_get('provisionator_apache_target_dir','/Library/WebServer/Documents'),
                '#description' => t('Directory where the symlink to expose the new site should be made. It will point to the Drupal directory.'),
                );

        $form['provisionator_manifest_index_filename'] = array(
                '#type' => 'textfield',
                '#title' => 'Manifest Index File Location',
                '#size' => 30,
                '#maxlength' => 512,
                '#required' => TRUE,
                '#default_value' => variable_get('provisionator_manifest_index_filename','/Library/WebServer/Documents/provisionator.html'),
                '#description' => t('Location of the file to be used as an index of all provisioned sites. Handy for a table of contents for the server, etc... If you don\'t really want one, stick it in /tmp/output.html or something.'),
                );




        return $form;
}

/**
 * Implementation of hook_perm().
 */
function provisionator_perm() {
        return array(
                'administer provisionator', 
                'deploy new site', 
                'freezedry deployed site', 
                'delete own deployed sites', 
                'administer deployed sites', 
                'edit own deployed sites'
        );
}


/**
 * Implementation of hook_cron().
 */
function provisionator_cron() {

}


/**
 * Implementation of hook_node_access_records().
 */
function provisionator_node_access_records($node) {

}


/**
 * Implementation of hook_node_grants().
 */
function provisionator_node_grants($account, $op) {

}


/**
 * Implementation of hook_node_operations().
 */
function provisionator_node_operations() {

}


/**
 * Implementation of hook_nodeapi().
 */
function provisionator_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
        switch ($op) {
        case 'delete revision':
                // TODO: Enter revision deletion query here, for example:
                // db_query('DELETE FROM {node_example} WHERE vid = %d', $node->vid);
                break;
        }
}


/**
 * Implementation of hook_node_info().
 */
function provisionator_node_info() {
        return array(
                'provisionatorsite' => array('name' => t('provisionator deployed site'), 'base' => 'provisionatorsite'),
                'provisionatorclient' => array('name' => t('provisionator client'), 'base' => 'provisionatorclient')
        );
}




function provisionator_views_tables() {

        $tables['provisionatorsite'] = array(
                "name" => "provisionatorsite",
                "join" => array(
                        "left" => array(
                                "table" => "node",
                                "field" => "nid"
                        ),
                        "right" => array(
                                "field" => "nid"
                        ),
                ),
                "fields" => array(
                        "shortname" => array(
                                'name' => "Provisionator: TEST Site Shortname",
                                'sortable' => true,
                        ),
                        "sitestatus" => array(
                                'name' => "Provisionator: Site Status",
                                'sortable' => true,
                        ),
                        "fullurl" => array(
                                'name' => "Provisionator: Site URL",
                                'sortable' => true,
                        ),
                        "profile" => array(
                                'name' => "Provisionator: Site Profile",
                                'sortable' => true,
                        ),
						"clientid" => array(
							'name' => "Provisionator: Client ID",
							'sortable' => true,
						),
                ),
                "sorts" => array(
                        "shortname" => array( 'name' => "Shortname" ),
                        "sitestatus" => array( 'name' => "Status" ),
                        "fullurl" => array( 'name' => "Site URL" ),
                        "profile" => array( 'name' => "Site Profile" ),
						"clientid" => array( 'name' => 'Client ID' ),
                ),
                "filters" => array(
					"clientid" => array(
						'name' => 'Provisionator: Client ID',
						'help' => t('ID of the Provisionator Client to restrict view list to.'),
						'field' => 'clientid',
						'operator' => array("="),
						'value' => 'integer',
					),
                ),
        );
        return $tables;
}

function provisionator_views_arguments() {
	$arguments['clientid'] = array(
      'name' => t('Provisionator: Client ID'),
      'handler' => 'views_handler_clientid',
      'option' => array(
        '#type' => 'select',
        '#options' => array('equal', 'not equal'),
      ),
      'help' => t('This argument is a single Node ID.'),
	);
	
	return $arguments;
}


function views_handler_clientid($op, &$query, $argtype, $arg = '') {
  switch($op) {
    case 'summary':
      $query->add_field('nid');
      $query->add_field("title");
      $fieldinfo['field'] = 'node.nid';
      return $fieldinfo;
    case 'sort':
      // do nothing here.
      break;
    case 'filter':
      $args = _views_break_phrase($arg);
      if ($args[0] == 'and') {
        $operator = $argtype['options'] ? '!=' : '=';
        foreach ($args[1] as $arg) {
          $query->add_where("provisionatorsite.clientid $operator %d", $arg);
        }
      }
      else {
        $query->add_where("provisionatorsite.clientid IN (%s)", implode(',', $args[1]));
      }
      break;
    case 'link':
      return l($query->title, "$arg/$query->nid");
    case 'title':
      list($type, $info) = _views_break_phrase($query);
      if (!$info) {
        return t('Untitled');
      }
      $nids = implode(',', $info); // only does numbers so safe

		//$result = db_query("select * from node,provisionatorsite where (node.nid = provisionatorsite.nid) and (provisionatorsite.clientid in (%s))", $nids)
      $result = db_query("SELECT title FROM {node} WHERE nid IN (%s)", $nids);
      while ($node = db_fetch_object($result)) {
        $title .= ($title ? ($type == 'or' ? ' + ' : ', ') : '') . check_plain($node->title);
      }
      return $title;
  }
}
